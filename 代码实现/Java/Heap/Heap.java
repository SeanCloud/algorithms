package com.sean.leetcode.test;

import java.util.Arrays;

// 大顶堆
public class Heap {
    private int[] tree;         // 数组实现的完全二叉树
    private int capacity;       // 容量
    private int currentLength;  // 当前数组已使用长度

    /**
     * 构造函数
     * @param capacity 初始容量
     */
    public Heap(int capacity) {
        this.tree = new int[capacity];
        this.capacity = capacity;
        this.currentLength = 0;
    }

    /**
     * 添加元素
     * @param value 待添加元素
     */
    public void offer(int value){
        if(this.currentLength >= this.capacity){    // 数组已耗尽，扩增数组为原来的两倍
            this.grow();
        }
        int cur = this.currentLength++;             // 获得待添加元素的添加位置
        if(cur == 0){                               // 当前堆为空直接添加
            this.tree[cur] = value;
        }else{                                      // 当前堆不为空，添加之后要向上调整
            this.tree[cur] = value;
            int p = cur;
            int parent = this.getParentIndex(p);
            while(this.tree[parent] < this.tree[p]){
                this.swap(parent, p);
                p = parent;
                parent = this.getParentIndex(p);
            }
        }
    }

    /**
     * 取出最大元素
     * @return 最大元素
     */
    public int poll(){
        if(isEmpty()){
            throw new RuntimeException("堆为空，无法取出更多元素！");
        }
        int cur = --this.currentLength;         // 获得当前堆尾
        int result = this.tree[0];              // 取出最大元素
        this.tree[0] = this.tree[cur];          // 将堆尾移到堆头
        if(cur != 0){                           // 如果取出的不是最后一个元素，需要向下调整堆
            int p = 0;
            int left = getLeftIndex(p);
            int right = getRightIndex(p);
            // 由于是数组实现，数组元素无法擦除，需要通过边界进行判断堆的范围
            // 当前节点和左节点在堆的范围内，
            while(p < this.currentLength &&
                    0 <= left && left < this.currentLength &&
                    (this.tree[left] > this.tree[p] || this.tree[right] > this.tree[p])){
                if(right >= this.currentLength){    // 当前节点没有右节点
                    if(this.tree[left] > this.tree[p] ){        // 左节点大于当前节点
                        swap(p, left);
                        p = left;
                    }
                }else{                                          // 两个节点都在堆范围
                    if(this.tree[left] > this.tree[right]){     // 用大的节点替换
                        swap(p, left);
                        p = left;
                    }else{
                        swap(p, right);
                        p = right;
                    }
                }
                left = getLeftIndex(p);
                right = getRightIndex(p);
            }
        }
        return result;
    }

    public boolean isEmpty(){
        return this.currentLength <= 0;
    }

    private int getParentIndex(int index){
        return (index - 1) / 2;
    }

    private int getLeftIndex(int index){
        return 2 * index + 1;
    }

    private int getRightIndex(int index){
        return 2 * index + 2;
    }

    private void swap(int left, int right){
        int temp = this.tree[left];
        this.tree[left] = this.tree[right];
        this.tree[right] = temp;
    }

    /**
     * 将数组拓展为原来的两倍
     */
    private void grow(){
        this.tree = Arrays.copyOf(this.tree, 2 * currentLength);
        this.capacity = this.tree.length;
    }
}